iohmma
======

A C# implementation of an Input-Output Hidden Markov Model.

Naming
------

IOHMMa stands for <b>I</b>nput-<b>O</b>utput <b>H</b>idden <b>M</b>arkov <b>M</b>odel. The **a** makes the abbreviation sound like [Iowa](https://en.wikipedia.org/wiki/Iowa)

Technical report
----------------

Van Onsem, W., Demoen, B. and De Causmaecker, P.; *Input-output Hidden Markov Models*, CW report, May 2014 (forthcoming)
